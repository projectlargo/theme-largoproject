<?php get_header(); ?>

			<div id="content" class="homepage-content clearfix span8">
					<div class="homepage-1col top">
						<p class="intro"><strong>Project Largo</strong> is a WordPress framework developed by the <a href="http://investigativenewsnetwork.org">Investigative News Network</a> that is specifically designed for news publishers. Sound interesting? <a href="https://github.com/INN/Largo">Check out our code on Github</a> or <a href="/contact/">get in touch</a> to setup a demo.</p>
						<p>Largo incorporates a number of features from NPR's <a href="http://argoproject.org">Project Argo</a> but has been significantly expanded to include a number of additional features to make it easier to set up and more flexible to better support the needs of a variety of news publishers.</p>
						<p><strong>Status:</strong> We now have 100+ sites using Largo and we're continuing to improve the platform every day! <a href="/contact/">Drop us a note</a> if you'd like to receive updates on our progress or are interested in contributing to the project.</p>
						<h3>Key Features</h3>

						<p><strong>Responsive design</strong> to accommodate a variety of devices, screen sizes and resolutions</p>
						<p><strong>Flexible homepage layout options</strong> to support different news site presentation needs <span class="small">(Click to view larger)</span></p>


						<a href="<?php bloginfo( 'stylesheet_directory' ); ?>/img/blog-lg.png" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/blog-sm.png"  alt="blog layout" /></a>
						<a href="<?php bloginfo( 'stylesheet_directory' ); ?>/img/news-lg.png" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/news-sm.png" alt="news layout" /></a>
						<a href="<?php bloginfo( 'stylesheet_directory' ); ?>/img/carousel-lg.png" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/carousel-sm.png" alt="carousel layout" /></a>

						<p><strong>Live examples:</strong></p>

						<a href="http://jjie.org" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/jjie.png"  alt="Juvenile Justice Information Exchange" /></a>

						<a href="http://aspenjournalism.org" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/aspen.png" alt="Aspen Journalism" /></a>

						<a href="http://thelensnola.org" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/lens.png"  alt="The Lens NOLA" /></a>

						<a href="http://gijn.org" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/gijn.png" alt="Global Investigative Journalism Network" /></a>

						<a href="http://c-hit.org" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/c-hit.png"  alt="Connecticut Health I-Team" /></a>

						<a href="http://pinetreewatchdog.org" target="_blank"><img class="homepage-screenshot" src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/pinetree.png" alt="Pine Tree Watchdog" /></a>

						<p><strong>Highly-configurable responsive navigation menus</strong> built with the needs of news publishers in mind</p>
						<p><strong>Built-in support</strong> for donate buttons, social media integration, Google Analytics, custom bylines, author profiles and other functionality that would normally require additional plugins</p>
						<p><strong>Fancy post formatting</strong> including pull quotes, asides, images and slideshows with captions and credits, ability to break articles into multiple pages, heading and body text optimized for readability and support for a variety of embedded media</p>
						<p><strong>Integrated support </strong>for Open Graph, Twitter Cards, Schema.org metadata, hnews and hcard microformats</p>
						<p><strong>Curated list of plugins</strong> for add-on functionality such as enhanced editorial workflow, search engine optimization, caching and backup and more sophisticated comment management</p>
						<p><strong>Open source</strong> so you can use Largo as a complete solution or as a starting point for more complex projects</p>
						<p><strong>And much more!</strong> <a href="https://github.com/INN/Largo">Download the latest version</a> or <a href="mailto:largo@investigativenewsnetwork.org">get in touch</a> to schedule a demo.</p>

						<h3>Download</h3>
						<p>You can <a href="https://github.com/INN/Largo">download or fork the latest version</a> of the Largo theme from Github.</p>
					</div>

					<div class="homepage-2col left">
						<h3>Largo Setup</h3>
						<ul>
							<li><a href="http://www.largoproject.org/setup/pre-launch-checklist/">Pre-launch Checklist</a></li>
							<li><a href="http://www.largoproject.org/setup/download-and-installation/">Download and Installation</a></li>
							<li><a href="http://www.largoproject.org/setup/theme-options/">Theme Options</a></li>
							<li><a href="http://www.largoproject.org/setup/menus/">Menus</a></li>
							<li><a href="http://www.largoproject.org/setup/sidebars-and-widgets/">Sidebars and Widgets</a></li>
							<li><a href="http://www.largoproject.org/setup/plugins/">Plugins</a></li>
							<li><a href="http://www.largoproject.org/setup/for-developers/">For Developers</a></li>
						</ul>
					</div>

					<div class="homepage-2col right">
						<h3>Post Formatting</h3>
						<ul>
							<li><a href="http://www.largoproject.org/the-basics/">All The Basics</a></li>
							<li><a href="http://www.largoproject.org/headings-and-paragraphs/">Headings and Paragraphs</a></li>
							<li><a href="http://www.largoproject.org/photos-and-video/">Photos and Video</a></li>
							<li><a href="http://www.largoproject.org/tabular-data/">Tabular Data</a></li>
							<li><a href="http://www.largoproject.org/lists/">Lists</a></li>
							<li><a href="http://www.largoproject.org/blockqoutes-and-pull-quotes/">Blockquotes and Pull Quotes</a></li>
							<li><a href="http://www.largoproject.org/page-breaks/">Page Breaks</a></li>
							<li><a href="http://www.largoproject.org/less-common-html-elements/">Less Common HTML Elements</a></li>
						</ul>
					</div>
				</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>